/**
 * @author medany
 */

/*
 * A number is called an Armstrong number if it is equal to the cube of its each
 * digit. for example, 153 is an Armstrong number because 153= 1+ 125+27 which
 * is equal to 1^3+5^3+3^3. You need to write a program to check if given number
 * is Armstrong number or not.
 */
class ArmstrongNumber {

	public static boolean isArmStrong(int number) {
		int result = 0;
		int orig = number;
		while (number != 0) {
			int remainder = number % 10;
			result = result + remainder * remainder * remainder;
			number = number / 10;
		}
		if (orig == result) {
			return true;
		}
		return false;
	}
}
