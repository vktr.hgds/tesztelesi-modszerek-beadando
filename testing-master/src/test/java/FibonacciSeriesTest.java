import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class FibonacciSeriesTest {

    @Test
    void testFibonacciRecursion_1() {
        BigInteger a = new BigInteger("1");
        BigInteger b = FibonacciSeries.fibonacciRecursion(1);
        assertEquals(a, b);
    }

    @Test
    void testFibonacciNoRecursion_1() {
        BigInteger a = new BigInteger("1");
        BigInteger b = FibonacciSeries.fibonacciNoRecursion(1);
        assertEquals(a, b);
    }

    @Test
    void testFibonacciRecursion_0() {
        BigInteger a = new BigInteger("0");
        BigInteger b = FibonacciSeries.fibonacciRecursion(0);
        assertEquals(a, b);
    }

    @Test
    void testFibonacciNoRecursion_0() {
        BigInteger a = new BigInteger("0");
        BigInteger b = FibonacciSeries.fibonacciNoRecursion(0);
        assertEquals(a, b);
    }

    @Test
    void testFibonacciRecursion_more() {
        BigInteger a = new BigInteger("8");
        BigInteger b = FibonacciSeries.fibonacciRecursion(6);
        assertEquals(a, b);
    }

    @Test
    void testFibonacciNoRecursion_more() {
        BigInteger a = new BigInteger("8");
        BigInteger b = FibonacciSeries.fibonacciNoRecursion(6);
        assertEquals(a, b);
    }
}