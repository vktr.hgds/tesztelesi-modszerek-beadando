import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseWordsinStringTest {

    @Test
    void testreverseLine() {
        assertEquals("", ReverseWordsinString.reverseLine(""));
    }

    @Test
    void testreverseLine2() {
        assertEquals(null, ReverseWordsinString.reverseLine(null));
    }

    @Test
    void testreverseLine3() {
        assertEquals("amla", ReverseWordsinString.reverseLine("amla"));
    }
}