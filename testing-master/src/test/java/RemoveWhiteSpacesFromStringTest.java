import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveWhiteSpacesFromStringTest {

    @Test
    void testRemoveWhiteSpacesFromString() {
        String str = "aaa";
        assertEquals(str, RemoveWhiteSpacesFromString.removeWhiteSpacesFromString("a a a"));
    }
}