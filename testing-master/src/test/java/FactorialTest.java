import java.math.BigInteger;

import static org.junit.Assert.*;

public class FactorialTest {

    @org.junit.Test
    public void testFactZero() {
        BigInteger a = new BigInteger("1");
        BigInteger b = Factorial.fact(new BigInteger("0"));
        assertEquals(a, b);
    }

    @org.junit.Test
    public void testFact() {
        BigInteger a = new BigInteger("120");
        BigInteger b = Factorial.fact(new BigInteger("5"));
        assertEquals(a, b);
    }

    @org.junit.Test
    public void testFact2Zero() {
        BigInteger a = new BigInteger("1");
        BigInteger b = Factorial.Fact(new BigInteger("0"));
        assertEquals(a, b);
    }

    @org.junit.Test
    public void testFact2() {
        BigInteger a = new BigInteger("120");
        BigInteger b = Factorial.Fact(new BigInteger("5"));
        assertEquals(a, b);
    }

}