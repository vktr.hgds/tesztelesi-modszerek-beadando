import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class FindSquareRootTest {

    @Test
    void testfindSqrt() {
        BigDecimal a = new BigDecimal("10.0");
        BigDecimal b = FindSquareRoot.findSqrt(new BigDecimal("100"));
        assertEquals(a,b);
    }
}