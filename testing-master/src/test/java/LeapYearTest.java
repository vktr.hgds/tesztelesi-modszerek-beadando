import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LeapYearTest {

    @Test
    void testisLeapYear() {
        assertTrue(LeapYear.isLeapYear(2020));
    }

    @Test
    void isLeapYear2() {
        assertFalse(LeapYear.isLeapYear(2019));
    }

    @Test
    void testisleapYear() {
        assertTrue(LeapYear.isleapYear(2020));
    }

    @Test
    void testisleapYear2() {
        assertFalse(LeapYear.isleapYear(2019));
    }
}