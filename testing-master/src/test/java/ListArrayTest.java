import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ListArrayTest {

    @Test
    void testadd() {
        ListArray<Integer> listArray = new ListArray<>();
        listArray.add(1);
        listArray.add(2);
        assertEquals(10, listArray.getlistSize());
    }

    @Test
    void testremove() {
        ListArray<Integer> listArray = new ListArray<>();
        listArray.add(1);
        listArray.add(2);
        listArray.remove(1);
        assertEquals(10, listArray.getlistSize());
    }

    @Test
    void testremove1() {
        ListArray<Integer> listArray = new ListArray<>();
        listArray.add(1);
        listArray.add(2);
        listArray.remove(4);
        assertEquals(10, listArray.getlistSize());
    }



    @Test
    void testget1() {
    }

    @Test
    void testelementCount() {
        ListArray<Integer> listArray = new ListArray<>();
        listArray.add(1);
        listArray.add(2);
        assertEquals(2, listArray.getelementCount());
    }

    @Test
    void testlistSize() {
    }

    @Test
    void testclear() {
        ListArray<Integer> listArray = new ListArray<>();
        listArray.add(1);
        listArray.add(2);
        listArray.clear();
        assertEquals(0, listArray.getelementCount());
    }
}