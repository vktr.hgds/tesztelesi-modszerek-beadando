import org.junit.jupiter.api.Assertions;

class ArmstrongNumberTest {

    @org.junit.jupiter.api.Test
    void testisArmStrongInvalid() {
        short armstrongNumber = 153;
        Assertions.assertTrue(ArmstrongNumber.isArmStrong(armstrongNumber));
    }

    @org.junit.jupiter.api.Test
    void testisArmStrongValid() {
        Assertions.assertFalse(ArmstrongNumber.isArmStrong(157));
    }

}