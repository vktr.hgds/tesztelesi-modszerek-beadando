import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SearchTreeTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void testgetRoot() {
        SearchTree tree = new SearchTree(null);
        assertEquals(null, tree.getRoot());
    }

    @Test
    void testaddItem() {
        SearchTree tree = new SearchTree(null);
        String stringData = "5 7 3 9 8 2 1 0 4 6";

        String[] data = stringData.split(" ");
        for (String s : data) {
            tree.addItem(new Node(s));
        }
        tree.traverse(tree.getRoot());
        assertEquals(new Node("5").getValue(), tree.getRoot().getValue());
    }

    @Test
    void testremoveItem() {
        SearchTree tree = new SearchTree(null);
        String stringData = "5 2 6 7";

        String[] data = stringData.split(" ");
        for (String s : data) {
            tree.addItem(new Node(s));
        }
        tree.removeItem(null);
        tree.traverse(tree.getRoot());
        assertEquals(new Node("5").getValue(), tree.getRoot().getValue());
    }

}