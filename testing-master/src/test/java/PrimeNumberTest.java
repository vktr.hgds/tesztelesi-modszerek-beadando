import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimeNumberTest {

    @Test
    void testisPrimeZero() {
        assertFalse(PrimeNumber.isPrime(0));
    }

    @Test
    void testisPrimeTwo() {
       assertTrue(PrimeNumber.isPrime(2));
    }

    @Test
    void testisPrimePair() {
        assertFalse(PrimeNumber.isPrime(10));
    }

    @Test
    void testisPrimeNotPair1() {
        assertTrue(PrimeNumber.isPrime(11));
    }

    @Test
    void testisPrimeNotPair2() {
        assertFalse(PrimeNumber.isPrime(9));
    }
}