import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTest {

    @Test
    void testbinarySearch() {
        int[] array = new int[3];
        array[0] = 1;
        array[1] = 3;
        array[2] = 2;
        int res = BinarySearch.binarySearch(3, array);
        assertEquals(1,res);
    }

    @Test
    void testbinarySearchv2() {
        int[] array = new int[3];
        array[0] = 1;
        array[1] = 3;
        array[2] = 2;
        int res = BinarySearch.binarySearch(2, array);
        assertEquals(-1,res);
    }
}