import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class IntegerPalindromeTest {

    @Test
    void testIsPalindrome() {
        boolean palindrome = IntegerPalindrome.isPalindrome(new BigInteger("1001"));
        assertEquals(true, palindrome);
    }

    @Test
    void testIsPalindrome2() {
        boolean palindrome = IntegerPalindrome.isPalindrome(null);
        assertEquals(false, palindrome);
    }

    @Test
    void testIsPalindrome3() {
        boolean palindrome = IntegerPalindrome.isPalindrome(new BigInteger("1"));
        assertEquals(true, palindrome);
    }

    @Test
    void testisPalindrome2() {
        boolean palindrome = IntegerPalindrome.isPalindrome2(new BigInteger("1001"));
        assertEquals(true, palindrome);
    }

    @Test
    void testisPalindrome21() {
        boolean palindrome = IntegerPalindrome.isPalindrome(null);
        assertEquals(false, palindrome);
    }

    @Test
    void testreverse() {
        assertEquals(new BigInteger("1234"), IntegerPalindrome.reverse(new BigInteger("4321")));
    }
}