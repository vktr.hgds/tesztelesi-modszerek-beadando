import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringPalindromeTest {

    @Test
    void testisPalindrome() {
        assertTrue(StringPalindrome.isPalindrome("bob"));
    }


    @Test
    void testisPalindrome2() {
        assertFalse(StringPalindrome.isPalindrome("boba"));
    }

    @Test
    void testisPalindrome3() {
        assertFalse(StringPalindrome.isPalindrome(""));
    }

    @Test
    void testisPalindrome4() {
        assertFalse(StringPalindrome.isPalindrome(null));
    }

    @Test
    void testisPalindrome21() {
        assertTrue(StringPalindrome.isPalindrome2("bob"));
    }

    @Test
    void testisPalindrome22() {
        assertFalse(StringPalindrome.isPalindrome2("boba"));
    }

    @Test
    void testisPalindrome23() {
        assertFalse(StringPalindrome.isPalindrome2(""));
    }

    @Test
    void testisPalindrome24() {
        assertFalse(StringPalindrome.isPalindrome2(null));
    }

    @Test
    void testfact() {
        assertEquals(6068, StringPalindrome.fact("alma"));
    }
}