import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {

    @Test
    void testgetName() {
        assertEquals("team", new Team<BaseBallPlayer>("team").getName());
    }

    @Test
    void testaddPlayer() {
        Team<FootballPlayer> team = new Team<>("ManUtd");
        team.addPlayer(new FootballPlayer("Roooney"));
        assertEquals(1, team.numPlayers());
    }

    @Test
    void testnumPlayers() {
        Team<FootballPlayer> team = new Team<>("ManUtd");
        team.addPlayer(new FootballPlayer("Roooney"));
        assertEquals(1, team.numPlayers());
    }

    @Test
    void testmatchResult() {
        Team<FootballPlayer> team = new Team<>("ManUtd");
        team.addPlayer(new FootballPlayer("Roooney"));
        Team<FootballPlayer> team2 = new Team<>("ManUtd2");
        team.addPlayer(new FootballPlayer("Roooney2"));

        team.matchResult(team2, 3, 2);
        assertEquals(1, team.won);
    }

    @Test
    void testranking() {
        Team<FootballPlayer> team = new Team<>("ManUtd");
        team.addPlayer(new FootballPlayer("Roooney"));
        Team<FootballPlayer> team2 = new Team<>("ManUtd2");
        team.addPlayer(new FootballPlayer("Roooney2"));

        team.matchResult(team2, 3, 2);
        assertEquals(2, team.ranking());
    }

}