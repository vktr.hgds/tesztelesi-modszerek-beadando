import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class FindGCDTest {

    @Test
    void testfindGCD() {
        assertEquals(new BigInteger("2"), FindGCD.findGCD(new BigInteger("4"), new BigInteger("2")));
    }

    @Test
    void testfinDGCD() {
        assertEquals(new BigInteger("2"), FindGCD.finDGCD(new BigInteger("4"), new BigInteger("2")));
    }
}