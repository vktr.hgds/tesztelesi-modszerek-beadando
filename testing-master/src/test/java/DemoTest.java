import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class DemoTest {

    @Test
    void testaddInOrder() {
        LinkedList<String> l = new LinkedList();
        l.add("bp");
        l.add("kecskemet");
        assertEquals(true, Demo.addInOrder(l, "Szeged"));
    }
}