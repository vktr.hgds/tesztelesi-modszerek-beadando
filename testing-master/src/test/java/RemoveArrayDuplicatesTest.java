import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class RemoveArrayDuplicatesTest {

    @Test
    void testremoveDuplicates() {
        Object[] arr = new Object[5];
        arr[0] = 1; arr[1] = 1; arr[2]= 2; arr[3] = 2; arr[4] = 3;
        assertEquals("[1, null, 2, null, 3]", Arrays.toString(RemoveArrayDuplicates.removeDuplicates(arr)));
    }
}