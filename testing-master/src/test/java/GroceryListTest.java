import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GroceryListTest {

    @Test
    void testaddGroceryItem() {
        GroceryList g = new GroceryList(); g.addGroceryItem("alma");
        assertEquals(1, g.getGroceryList().size());
    }

    @Test
    void testgetGroceryList() {
        GroceryList g = new GroceryList(); g.addGroceryItem("alma");
        assertEquals(1, g.getGroceryList().size());
    }

    @Test
    void testmodifyGroceryItem() {
        GroceryList g = new GroceryList(); g.addGroceryItem("alma");
        g.modifyGroceryItem("alma", "banan");
        assertEquals("banan", g.getGroceryList().get(0));
    }

    @Test
    void testremoveGroceryItem() {
        GroceryList g = new GroceryList(); g.addGroceryItem("alma");
        g.removeGroceryItem("alma");
        assertEquals(0, g.getGroceryList().size());
    }
}