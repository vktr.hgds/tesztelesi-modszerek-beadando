import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringAnagramTest {

    @Test
    void testisAnagram() {
        assertTrue(StringAnagram.isAnagram("alma", "mala"));
    }

    @Test
    void testisAnagraam() {
        assertTrue(StringAnagram.isAnagraam("alma", "mala"));
    }

    @Test
    void testisAnagraminvalid() {
        assertFalse(StringAnagram.isAnagram("alma", "ala"));
    }

    @Test
    void testisAnagraaminvalid() {
        assertFalse(StringAnagram.isAnagraam("alma", "mla"));
    }

    @Test
    void testisAnagraminvalid2() {
        assertFalse(StringAnagram.isAnagram("alma", "nabb"));
    }

    @Test
    void testisAnagraaminvalid2() {
        assertFalse(StringAnagram.isAnagraam("alma", "bnaa"));
    }
}